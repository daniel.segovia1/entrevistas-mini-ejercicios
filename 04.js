const auto = {
    marca : "volkswagen",
    modelo: "golf",
    fullname: function(){
        return `El nombre completo es ${this.marca} - ${this.modelo}` 
    }
}

const auto2 = {
    marca : "volkswagen",
    modelo: "golf",
    fullname: () => {
        return `El nombre completo es ${this.marca} - ${this.modelo}` 
    }
}

console.log(auto.fullname())
console.log(auto2.fullname())

